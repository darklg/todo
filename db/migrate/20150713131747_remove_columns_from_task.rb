class RemoveColumnsFromTask < ActiveRecord::Migration
  def change
	remove_column :tasks, :finished_date
	remove_column :tasks, :finished
  end
end
