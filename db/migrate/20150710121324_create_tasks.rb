class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title, :null => false
      t.text :description, :null => false
      t.timestamp :created_date, :null => false
      t.timestamp :finished_date, :null => false
      t.boolean :finished, :null => false, :default => false

      t.timestamps null: false
    end
  end
end
