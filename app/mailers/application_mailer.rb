class ApplicationMailer < ActionMailer::Base
  default from: "ciprojektwimiip@gmail.com"
  layout 'mailer'
end
