class UserMailer < ApplicationMailer
	default from: 'ciprojektwimiip@gmail.com'

	def welcome_email(user)
		@user = user
		mail(to: @user.email, subject: 'Welcome to task list site!')
	end

	def reset_password_mail(user)
		@user = user
		mail(to: @user.email, subject: 'Change password')
	end
end
