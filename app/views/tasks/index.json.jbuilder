json.array!(@tasks) do |task|
  json.extract! task, :id, :title, :description, :created_date, :finished_date, :finished
  json.url task_url(task, format: :json)
end
