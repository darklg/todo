require 'securerandom'
require 'digest'
class UsersController < ApplicationController
	before_action :check_email_and_user, only: [:reset_password]
	before_action :check_passwords, only: [:commit_change_password]
	before_action :authenticate, except: [:new, :create, :forgot_password, :reset_password]
	
	def index
		@users = User.all
	end
	def show
	end
	
	def new
		if !logged_in?
			@user = User.new
		else
			redirect_to root_path, :notice => "You have already an account"
		end
	end
	
	def edit
		@user = current_user
		if @user == nil
			redirect_to root_path, :notice => "logged"
		end
	end

	def create
		if !logged_in?
			@user = User.new(user_params)
			respond_to do |format|
				if @user.save
					UserMailer.welcome_email(@user).deliver_now!
					format.html { redirect_to login_path, notice: 'User was succesdljfk'}
					format.json { render :show, status: :created, location: @user }
				else
					format.html { render :new }
					format.json { render json: @user.errors, status: :unprocessable_entity }
				end
			end
		end
	end
	
	def update
		@user = current_user
		respond_to do |format|
			if @user.update(user_params)
				format.html { redirect_to @user, notice: 'User was succesdljfk'}
				format.json { render :show, status: :ok, location: @user }
			else
				format.html { render :edit }
				format.json { render json: @user.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@user = current_user
		Task.where(:user_id => @user.id).destroy_all
		@user.destroy
		redirect_to logout_path, :notice => "Your account has been removed."
		
	end
	def forgot_password
	end
	
	def reset_password
	end
	
	def change_password
	end

	def commit_change_password
		if (params[:new_password] != params[:password_confirmation]) 
			redirect_to change_pass_path, :notice => "Passwords don't match"
		elsif  !(params[:new_password].length.between?(4,20))
			redirect_to change_pass_path, :notice => "Passwords must have at least 4 signs and less than 20"
		else
			@user = current_user
			if Digest::SHA1.hexdigest(params[:old_password]) == @user.hashed_password
				@user.update_attribute(:hashed_password, Digest::SHA1.hexdigest(params[:new_password]))
				redirect_to tasks_path, :notice => "Your password has been changed."
			else
				redirect_to change_pass_path, :notice => "Old password is incorrect"
			end
		end
	end

	private
	def set_user
		@user = User.find(params[:id])
	end
	
	def check_passwords
		
	end
	
	def check_email_and_user
		if /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/.match(params[:email])
			@user = User.find_by_email(params[:email])
			if @user == nil
				redirect_to forgot_path, :notice => "There is no user in database with given email"
			else
				newpass = SecureRandom.urlsafe_base64(15)
				@user.hashed_password = newpass
				UserMailer.reset_password_mail(@user).deliver_now!
				@user.update_attribute(:hashed_password, Digest::SHA1.hexdigest(newpass))
				redirect_to login_path, :notice => "Your password has been changed. Please check your email inbox"
			end
		else
			redirect_to forgot_path, :notice => "Invalid email address"
		end
	end

	def user_params
		params.require(:user).permit(:email, :login, :password, :password_confirmation)
	end
end
