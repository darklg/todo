class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception
	  
	protected
		def current_user
			@current_user = nil
			if session[:user_id]
				@current_user ||= User.find_by_id(session[:user_id])
			else
				@current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
			end
			@current_user
		end
		
		helper_method :current_user
		
		def authenticate
			logged_in? || access_denied
		end
		
		def logged_in?
			current_user.is_a? User
		end
	
		helper_method :logged_in?
		
		def access_denied
			redirect_to login_path, notice: "Please log in to continue" and return false
		end
end
