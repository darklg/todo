class Task < ActiveRecord::Base
	validates :title, :description, presence: true
	validates :title, length: { :in => 4..100 }
	validates :description, length: { :in => 4..500 }
	validates :title, :description, :format => {:with => /\w+\s*\d*/, :message => "This field can contain only letters spaces and numbers." }
	self.per_page = 5
	belongs_to :user
end
