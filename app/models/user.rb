require 'digest'
require 'securerandom'
class User < ActiveRecord::Base
	attr_accessor :password
	
	has_many :tasks
	
	validates_uniqueness_of :email, :login
	validates_length_of :email, :within => 5..50
	validates :email, format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }
	validates_confirmation_of :password
	validates_length_of :password, :within => 4..20
	validates_presence_of :password, :if => :password_required?
	
	before_save :encrypt_new_password
	before_create { generate_token(:auth_token) }
	def self.authenticate(email, password)
		user = find_by_email(email)
		return user if user && user.authenticated?(password)
	end
	
	def authenticated?(password)
		self.hashed_password == encrypt(password)
	end
	
	protected
	def encrypt(string)
		Digest::SHA1.hexdigest(string)
	end	

	def encrypt_new_password
		return if password.blank?
		self.hashed_password = encrypt(password)
	end
	
	def password_required?
		hashed_password.blank? || password.present?
	end
	
	def generate_token(column)
		begin
			self[column] = SecureRandom.urlsafe_base64
		end while User.exists?(column => self[column])
	end
	
end
